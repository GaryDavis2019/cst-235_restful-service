package beans;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.faces.bean.*;

@ManagedBean(name="books", eager = true) 
@ViewScoped


public class Books implements Serializable {
	   private static final long serialVersionUID = 1L;
		//Create a list with temp data
	   private static final ArrayList<Book> books = new ArrayList<Book>(Arrays.asList(new Book("1","1","1","")));
		String bookNumber;
		String chapterNumber;
		String verseNumber;
		String verseText;
 
	//Bookss getter/setter	
	public List<Book> getBooks() {
		return books;
	}
	public void setOrderNumber(Book BookObj) {
		books.add(BookObj);
	}
	
}
