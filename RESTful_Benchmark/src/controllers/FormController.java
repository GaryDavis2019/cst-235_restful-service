package controllers;


import javax.faces.bean.*;
import javax.inject.*;

import beans.Book;
import business.BooksBusinessInterface;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


@ManagedBean 
@SessionScoped
public class FormController {
	//Inject the Books Business Interface
	@Inject
	private business.BooksBusinessInterface service;

	
	//get the information from the service
	public  BooksBusinessInterface getService() {
		return service;
	}
	
	//Use a query to get the chapters for the book. The book number is passed to the method
	public List<String> getChapList(String bookStr){
		List<String> chapterList = new ArrayList<String>();
		//System.out.println("This is from the form "+bookStr);
		Connection conn = null;
		String url = "jdbc:postgresql://localhost:5432/";
		String username = "postgres";
		String password = "password";
		String stringStatement = "SELECT * FROM bible.kjv";
		try {
			//Connect to the database
			conn = DriverManager.getConnection(url, username, password);
			Statement statement  = conn.createStatement();
			ResultSet rs = statement.executeQuery(stringStatement);
			while(rs.next()) {
				//find all chapters for the book using an if statement
				if (rs.getString("BOOK").contentEquals(bookStr) && !chapterList.contains(rs.getString("CHAPTER"))) {
					chapterList.add(rs.getString("CHAPTER"));
				}
				
			}
			//Close the connection
			rs.close();
			conn.close();
			//return chapterList;
		} catch (SQLException e) {
			System.out.println("Failure");
			e.printStackTrace();
		}finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			}
		return chapterList;
	}
	
	
	//Use a query to get the verses for the book/chapters. The book and chapter numbers are passed to the method
	public List<String> getVerseList(String bookStr, String chapStr){
		List<String> verseList = new ArrayList<String>();
		Connection conn = null;
		String url = "jdbc:postgresql://localhost:5432/";
		String username = "postgres";
		String password = "password";
		String stringStatement = "SELECT * FROM bible.kjv";
		try {
			//Connect to the database
			conn = DriverManager.getConnection(url, username, password);
			Statement statement  = conn.createStatement();
			ResultSet rs = statement.executeQuery(stringStatement);
			while(rs.next()) {
				//Use if statement to get the verse numbers based on the book and chapter
				if (rs.getString("BOOK").contentEquals(bookStr) && rs.getString("CHAPTER").contentEquals(chapStr) && !verseList.contains(rs.getString("VERSE"))) {
					verseList.add(rs.getString("VERSE")); //Add to the verseList
					//System.out.println(rs.getString("VERSE"));
				}
				
			}
			//Close the connection
			rs.close();
			conn.close();
			//return verseList;
		} catch (SQLException e) {
			System.out.println("Failure");
			e.printStackTrace();
		}finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		
			}
		return verseList;
	}	
	
	//Query used to get the verse text. Method needs the book, chapter and verse
	public String getVerseText(String bookStr, String chapStr, String verStr){
		List<Book> newBookList = new ArrayList<Book>();
		String verseText = "";
		Connection conn = null;
		String url = "jdbc:postgresql://localhost:5432/";
		String username = "postgres";
		String password = "password";
		String stringStatement = "SELECT * FROM bible.kjv";
		try {
			//Connection to the database
			conn = DriverManager.getConnection(url, username, password);
			Statement statement  = conn.createStatement();
			ResultSet rs = statement.executeQuery(stringStatement);
			while(rs.next()) {
				//If statement used to get the verse text. Uses the Book#, Chapter# and Verse#
				if (rs.getString("BOOK").contentEquals(bookStr) && rs.getString("CHAPTER").contentEquals(chapStr) && rs.getString("VERSE").contentEquals(verStr)) {
					verseText = rs.getString("VERSE_TEXT"); //This is the verse text
					//Object is created and added to a local list
					newBookList.add(new Book(bookStr,chapStr,verStr,verseText));
				}
			}
			//Close the connection
			rs.close();
			conn.close();
			//return verseList;
		} catch (SQLException e) {
			System.out.println("Failure");
			e.printStackTrace();
		}finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		
			}
		//local list will be passed to the setBook Method (service will be updated with new list)
		service.setBooks(newBookList);
		return verseText;
	}
	public void valueChanged() {
	    //do your stuff
		System.out.println("event " );
		//getChapList(String bookStr)
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}


