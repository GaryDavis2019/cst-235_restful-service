package controllers;

//import java.io.IOException;

import javax.faces.bean.*;
//import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.*;
import beans.User;
import business.OrdersBusinessInterface;

import java.sql.*;

import javax.ejb.EJB;

@ManagedBean 
@ViewScoped
public class FormController {
	@Inject
	private business.OrdersBusinessInterface service;
	@EJB 
	business.MyTimerService timer;
	
	//Submit button clicked in form and displays the User first and last name
	public String onSubmit(User user) {
		//System.out.println(user.getFirstName()); //print the user first name on console
		//System.out.println(user.getLastName());  //print the user first name on console
		
		service.test(); //prints a test message from either the OrdersBusinessService and AnotherOrdersBusinessService.
		timer.setTimer(10000); //This will display a delayed message 10 seconds after the submit button was pushed.
		getAllOrders();
		insertOrder();
		getAllOrders();
		return "TestResponse.xhtml"; //Forwarding to new form page
		
	}
	
	//Testing redirect
	public String onFlash(User user){
		//ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		System.out.println(FacesContext.getCurrentInstance().getClientIdsWithMessages());
		getRequestMap();
		return "TestResponse.xhtml?faces-redirect=true";
		
	}
	
	public String getRequestMap() {
		return "TestResponse.xhtml";
		
	}
	
	//get the information from the service
	public  OrdersBusinessInterface getService() {
		return service;
	}
	
	private void getAllOrders(){
		Connection conn = null;
		String url = "jdbc:postgresql://localhost:5432/";
		String username = "postgres";
		String password = "password";
		String stringStatement = "SELECT * FROM testapp.ORDERS";
		try {
			conn = DriverManager.getConnection(url, username, password);
			System.out.println("Success");
			Statement statement  = conn.createStatement();
			ResultSet rs = statement.executeQuery(stringStatement);
			while(rs.next()) {
				System.out.println("ID:"+ rs.getInt("ID")+"; Product:"+rs.getString("PRODUCT_NAME")+"; Price:"+ rs.getFloat("PRICE")
				+ "; QUANT:"+rs.getString("QUANTITY"));
			}
			rs.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println("Failure");
			e.printStackTrace();
		}finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		
			}
	}
	
	private void insertOrder() {
		Connection conn = null;
		String url = "jdbc:postgresql://localhost:5432/";
		String username = "postgres";
		String password = "password";
		String stringStatement = "INSERT INTO  testapp.ORDERS(ORDER_NO, PRODUCT_NAME, PRICE, QUANTITY) VALUES('001122334455', 'This was inserted new', 25.00, 100)";
		try {
			conn = DriverManager.getConnection(url, username, password);
			System.out.println("Success");
			Statement statement  = conn.createStatement();
			statement.executeUpdate(stringStatement);

			conn.close();
		} catch (SQLException e) {
			System.out.println("Failure");
			e.printStackTrace();
		}finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		
			}
	}
	
}


