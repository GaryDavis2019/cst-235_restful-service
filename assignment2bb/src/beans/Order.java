package beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Order")
public class Order implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String orderNumber;
	String productName;
	float price;
	int quantity;
	
	/** Constructor */ 
	public Order() {
		this.orderNumber = "";
		this.productName = "";
		this.price = 0;
		this.quantity = 0;
	}
	
	
	public Order(String OrderNumber, String ProductName, float Price, int Quantity) {
		this.orderNumber = OrderNumber;
		this.productName = ProductName;
		this.price = Price;
		this.quantity = Quantity;
	}
	
	//Order Number getter/setter	
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String OrderNumber) {
		orderNumber = OrderNumber;
	}	

	//Product Name getter/setter	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String ProductName) {
		productName = ProductName;
	}
	
	//Price getter/setter	
	public float getPrice() {
		return price;
	}
	public void setPrice(float Price) {
		price = Price;
	}	
	
	//Quantity getter/setter	
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int Quantity) {
		quantity = Quantity;
	}	
	


}
