package business;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;

import beans.Order;

import javax.ejb.EJB;

/**
 * Message-Driven Bean implementation class for: OrderMessageService
 */
@MessageDriven(
		activationConfig = { @ActivationConfigProperty(
				propertyName = "destination", propertyValue = "java:/jms/queue/Order"), @ActivationConfigProperty(
				propertyName = "destinationType", propertyValue = "javax.jms.Queue")
		}, 
		mappedName = "java:/jms/queue/Order")
public class OrderMessageService implements MessageListener {
	@EJB
	business.OrdersBusinessInterface service;

    /**
     * Default constructor. 
     */
    public OrderMessageService() {
        // TODO Auto-generated constructor stub
    }
	
    /*This method is used to process a text message or object message.
      The text message will be printed to console
      The object message will be casted as an Order and used by the DAO to add to the database*/
    public void onMessage(Message message) {
    	if (message instanceof TextMessage) {
    		TextMessage tmpMessage = (TextMessage) message;
    		try {
				System.out.println(tmpMessage.getText());
			} catch (JMSException e) {
				e.printStackTrace();
			}
    	}
    	if (message instanceof ObjectMessage) {
    		ObjectMessage tmpMessage = (ObjectMessage) message;
    		System.out.println("I am the object Message ");
    		try {
    			Order tmp = (Order) tmpMessage.getObject();
    			
    			service.insertOrder(tmp);
			} catch (JMSException e) {
				e.printStackTrace();
			}
    	}
    }

}
